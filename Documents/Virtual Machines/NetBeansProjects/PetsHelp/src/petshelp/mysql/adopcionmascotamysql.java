
package petshelp.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import petshelp.clases.Conexion;
import petshelp.clases.mascotaadopcion;

public class adopcionmascotamysql extends Conexion{
    
    public boolean registrar(mascotaadopcion c){
        PreparedStatement ps = null;
        Connection con = conector();
        String sql ="INSERT INTO adopcion (idadopcion,nombremascota,dueño,fechanacimiento,raza,correo,color,telefono,direccion,sexo,fk_usuario,foto) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        try{
            ps=con.prepareStatement(sql);
            ps.setInt(1,c.getIdadopcion());
            ps.setString(2,c.getNombremascota());
            ps.setString(3,c.getDueño());
            ps.setString(4,c.getFechanacimiento());
            ps.setString(5,c.getRaza());
            ps.setString(6,c.getCorreo());
            ps.setString(7,c.getColor());
            ps.setInt(8,c.getTelefono());
            ps.setString(9, c.getDireccion());
            ps.setString(10,c.getSexo());
            ps.setInt(11, c.getFk_usuario());
            ps.setBinaryStream(12, c.getFoto());
            ps.execute();
            return true;
        }
        catch(SQLException ex){
        }        
        return false;
    }   
    
}
