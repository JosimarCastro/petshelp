
package petshelp.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import petshelp.clases.Conexion;
import petshelp.clases.Usuarios;

/**
 *
 * @author josst
 */
public class usuariosmysql extends Conexion{
    
    
        public boolean registrar(Usuarios c){
        PreparedStatement ps = null;
        Connection con = conector();
        String sql ="INSERT INTO usuario (id_usuario,usuario,nombre,sexo,direccion,codigopostal,telefono,correo,edad,contraseña,foto) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        try{
            ps=con.prepareStatement(sql);
           
            ps.setInt(1,c.getId());
            ps.setString(2,c.getUsuario());
            ps.setString(3,c.getNombre());
            ps.setString(4,c.getSexo());
            ps.setString(5,c.getDireccion());
            ps.setInt(6,c.getCodigopostal());
            ps.setInt(7,c.getTelefono());
            ps.setString(8, c.getCorreo());
            ps.setInt(9, c.getEdad());
            ps.setString(10, c.getContraseña());
            ps.setBinaryStream(11, c.getFoto());
            
            ps.execute();
            return true;
        }catch(SQLException ex){
        }        
        return false;
    }
}
