
package petshelp.mysql;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import petshelp.clases.Conexion;
import petshelp.clases.centroadopcion;

public class centrosadopcionmysql extends Conexion{
    
        public boolean registrar(centroadopcion c){
        PreparedStatement ps = null;
        Connection con = conector();
        String sql ="INSERT INTO centrosdeadopcion (id_centro,nombre,direccion,codigopostal,correo,telefono,fk_usuario,foto) VALUES(?,?,?,?,?,?,?,?)";
        try{
            ps=con.prepareStatement(sql);
            ps.setInt(1,c.getId_centro());
            ps.setString(2,c.getNombre());
            ps.setString(3,c.getDireccion());
            ps.setInt(4,c.getCodigopostal());
            ps.setString(5, c.getCorreo());
            ps.setInt(6,c.getTelefono());
            ps.setInt(7,c.getFk_usuario());
            ps.setBinaryStream(8, c.getFoto());
            ps.execute();
            return true;
        }
        catch(SQLException ex){
        }        
        return false;
    }   
    
}
