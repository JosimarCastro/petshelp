
package petshelp.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import petshelp.clases.Conexion;
import petshelp.clases.mascotaperdida;

public class mascotaperdidamysql extends Conexion{
    
        public boolean registrar(mascotaperdida c){
        PreparedStatement ps = null;
        Connection con = conector();
        String sql ="INSERT INTO mascotaperdida (idmascota,nombremascota,dueño,fechadeextravio,raza,manchas,color,telefono,direccion,sexo,fk_usuario,foto) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        try{
            ps=con.prepareStatement(sql);
            ps.setInt(1,c.getIdmascota());
            ps.setString(2,c.getMascota());
            ps.setString(3,c.getDueño());
            ps.setString(4,c.getFechaextravio());
            ps.setString(5,c.getRaza());
            ps.setString(6,c.getManchas());
            ps.setString(7,c.getColor());
            ps.setInt(8,c.getTelefono());
            ps.setString(9, c.getDireccion());
            ps.setString(10,c.getSexo());
            ps.setInt(11, c.getFk_usuario());
            ps.setBinaryStream(12, c.getFoto());
            ps.execute();
            return true;
        }
        catch(SQLException ex){
        }        
        return false;
    }   
}
