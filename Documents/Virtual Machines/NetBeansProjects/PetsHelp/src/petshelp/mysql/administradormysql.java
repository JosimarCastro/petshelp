package petshelp.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import petshelp.clases.Administrador;
import petshelp.clases.Conexion;
import petshelp.clases.Conexion;

public class administradormysql extends Conexion{

        
    public boolean registrar(Administrador c){
        PreparedStatement ps = null;
        Connection con = conector();
        String sql ="INSERT INTO administrador (id_administrador,usuario,nombre,sexo,direccion,codigopostal,telefono,correo,edad,contraseña) VALUES(?,?,?,?,?,?,?,?,?,?)";
        try{
            ps=con.prepareStatement(sql);
            ps.setInt(1,c.getId());
            ps.setString(2,c.getUsuario());
            ps.setString(3,c.getNombre());
            ps.setString(4,c.getSexo());
            ps.setString(5,c.getDireccion());
            ps.setInt(6,c.getCodigopostal());
            ps.setInt(7,c.getTelefono());
            ps.setString(8, c.getCorreo());
            ps.setInt(9, c.getEdad());
            ps.setString(10, c.getContraseña());
            
            ps.execute();
            return true;
        }
        catch(SQLException ex){
        }        
        return false;
    }   
}
