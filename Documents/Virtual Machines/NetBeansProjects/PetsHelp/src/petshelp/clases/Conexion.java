
package petshelp.clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Conexion {
        
    private static final String driver="com.mysql.jdbc.Driver";
    private static final String url="jdbc:mysql://localhost:3306/petshelp";
    private static final String user="root";
    private static final String pass="Choforo3d";    
    private static Connection con = null;
    
        public Connection conector(){
        con=null;
        try{
            Class.forName(driver);
            con=DriverManager.getConnection(url, user, pass);
            System.out.print("Conexion Exitosa");
            if (con!=null){
            }
        }
        catch (ClassNotFoundException | SQLException e){           
            System.out.print("Conexion Fallida");

        }
        return con;
    }

        public ResultSet ejecutarSQLSelect(String sql)
        {
            ResultSet resultado;       
            try {
                PreparedStatement sentencia = con.prepareStatement(sql);
                resultado = sentencia.executeQuery();          
                return resultado;
            } catch (SQLException ex) {        
                System.err.println("Error "+ex);    
                return null;
       }
    }
}
