
package petshelp.administrador;

import petshelp.sistema.MenuInicio;
import petshelp.administrador.AdmiAdministrador;
import petshelp.administrador.UsuariosAdministrador;


public class PaginaPrincipalAdmi extends javax.swing.JFrame {

    public PaginaPrincipalAdmi() {
        initComponents();
        this.setLocationRelativeTo(null);

    }
      
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        adopcion = new javax.swing.JToggleButton();
        extravio = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        adopcion1 = new javax.swing.JToggleButton();
        adopcion2 = new javax.swing.JToggleButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(945, 630));
        setResizable(false);
        getContentPane().setLayout(null);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 48)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/petshelp/Imagenes/logo.png"))); // NOI18N
        jLabel4.setText("PetsHelp");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(10, 20, 360, 70);

        jButton4.setBackground(new java.awt.Color(0, 153, 153));
        jButton4.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 14)); // NOI18N
        jButton4.setText("Centros De Adopción");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton4);
        jButton4.setBounds(50, 420, 250, 30);

        adopcion.setBackground(new java.awt.Color(255, 153, 0));
        adopcion.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 14)); // NOI18N
        adopcion.setText("Anuncios De Adopción");
        adopcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adopcionActionPerformed(evt);
            }
        });
        getContentPane().add(adopcion);
        adopcion.setBounds(620, 420, 260, 30);

        extravio.setBackground(new java.awt.Color(204, 204, 0));
        extravio.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 14)); // NOI18N
        extravio.setText("Mascotas Perdidas");
        extravio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                extravioActionPerformed(evt);
            }
        });
        getContentPane().add(extravio);
        extravio.setBounds(320, 420, 280, 30);

        jButton6.setBackground(new java.awt.Color(204, 0, 0));
        jButton6.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 14)); // NOI18N
        jButton6.setText("Cerrar Sesión");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton6);
        jButton6.setBounds(730, 50, 160, 30);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/petshelp/Imagenes/casa.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(50, 190, 250, 240);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/petshelp/Imagenes/casa_1.png"))); // NOI18N
        jLabel3.setText("jLabel3");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(310, 170, 290, 290);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/petshelp/Imagenes/casaaa_1.png"))); // NOI18N
        getContentPane().add(jLabel5);
        jLabel5.setBounds(500, 0, 898, 599);

        adopcion1.setBackground(new java.awt.Color(204, 0, 0));
        adopcion1.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 14)); // NOI18N
        adopcion1.setText("Administración de Usuarios");
        adopcion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adopcion1ActionPerformed(evt);
            }
        });
        getContentPane().add(adopcion1);
        adopcion1.setBounds(180, 520, 250, 30);

        adopcion2.setBackground(new java.awt.Color(51, 51, 255));
        adopcion2.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 14)); // NOI18N
        adopcion2.setText("Administradores");
        adopcion2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adopcion2ActionPerformed(evt);
            }
        });
        getContentPane().add(adopcion2);
        adopcion2.setBounds(510, 520, 250, 30);

        jLabel6.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 36)); // NOI18N
        jLabel6.setText("Página Principal");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(370, 30, 310, 60);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/petshelp/Imagenes/3676776.jpg"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, -30, 945, 630);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed

        CentroAdministrador a = new CentroAdministrador();
        a.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void extravioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_extravioActionPerformed

        MascotaPerdidaAdministrador a = new MascotaPerdidaAdministrador();
        a.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_extravioActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        MenuInicio a = new MenuInicio();
        a.setVisible(true);
        this.dispose();
          
    }//GEN-LAST:event_jButton6ActionPerformed

    private void adopcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adopcionActionPerformed
        // TODO add your handling code here:
        MascotadopcionAdministrador a = new MascotadopcionAdministrador();
        a.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_adopcionActionPerformed

    private void adopcion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adopcion1ActionPerformed
        // TODO add your handling code here:
        UsuariosAdministrador p = new UsuariosAdministrador();
        p.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_adopcion1ActionPerformed

    private void adopcion2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adopcion2ActionPerformed
        // TODO add your handling code here:
        AdmiAdministrador s = new AdmiAdministrador();
        s.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_adopcion2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PaginaPrincipalAdmi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PaginaPrincipalAdmi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PaginaPrincipalAdmi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PaginaPrincipalAdmi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PaginaPrincipalAdmi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton adopcion;
    private javax.swing.JToggleButton adopcion1;
    private javax.swing.JToggleButton adopcion2;
    private javax.swing.JButton extravio;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    // End of variables declaration//GEN-END:variables
}
